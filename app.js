var express = require('express'),
	app = express(),
    cors = require('cors'),
	server = require('http').createServer(app),
	io = require('socket.io').listen(server),
	mongoose = require('mongoose'),
	users = {};
	var path = require('path');
var gcm = require('node-gcm');
var  fs = require('fs');
var url = require('url');
// file Storage Start  
var multer  = require('multer');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'images/groupImages/')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now()+'-'+file.originalname)
  }
})
var upload = multer({ dest: 'images/' ,inMemory: true, storage: storage});
app.use('/groupImages',express.static(path.join(__dirname, 'images/groupImages')));
// File Storage End  

server.listen(3001,"0.0.0.0");
mongoose.connect('mongodb://localhost/chatData', function(err){
	if(err){
		console.log(err);
	} else{
		console.log('Connected to mongodb!');
	}
});

var chatSchema = mongoose.Schema({
	nick: String,//senderId
	msg: String,//receiverId
	img: String,
	receiver: String,
	imgFlag: Number,
	isImgDownloaded:{type: Number, default: 0},
	created: {type: Date, default: Date.now},
	chatId:String,
	readUnreadFlg:Number,
	groupId:String,
	groupOwnerId:Number,
	groupMemberId:String,
	groupName:String,
    senderName:String,
    senderImage:String,
	isBlocked:{type: Number, default: 0},
	loginUserId:String
});
var Chat = mongoose.model('Chat', chatSchema);
//CRUD group
var groupsSchema = mongoose.Schema(
{
    name:String, // Name of the group
    avtarImage:String, // Image of the group ( this will be uploaded physically and then stored as path)
    createdByName:String,
    createdById:Number, // creator of the group its phone number
    userId:String, // unique user id from couchdb
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
});
var Groups = mongoose.model('Group', groupsSchema);
// Crud Group Memebers
var groupsMemberSchema = mongoose.Schema(
{
    groupId:String, //  Group id  foreigh key from group table
    userNumber: String, // User phone number
    userId: String,    // userid which is foreign key from user table couchdb
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
});
var GroupsMembers = mongoose.model('GroupMember', groupsMemberSchema);

var registeruserSchema = mongoose.Schema(
{    
    userId: String,    // userid which is foreign key from user table couchdb
    status :String, 
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
});
var registeruser = mongoose.model('registeruser', registeruserSchema);


var userPushMessageSchema = mongoose.Schema(
{
    deviceToken: String, // Device Token
    sendMessage:{type: Number, default: 0}, // if user is in background then set to 1 if foreground then set to 0
});
var  userPushMessage = mongoose.model('userPushMsg', userPushMessageSchema);

// For sending push message
var userStatus = mongoose.Schema(
{
    userId:Number, //  user id which is couchdb user id
    userNumber: Number, // number of the phone whome to send
    sendMessage:{type: Number, default: 0},  // if 0 then dont send message if 1 then send push messages
    deviceToken: String  // device token for ios
});


app.post('/getFile', upload.single('uploadFile'), function (req, res, next) {
    console.log("file"+JSON.stringify(req.file));
    var file = req.file,
        path = './images/',
        mimetype = file.mimetype,
        fileName = file.originalname;
        if(fileName!='')
        {
            console.log('File saved successfully.');
            var data = {
                message:'File saved successfully.',
                errorCode : 200,
                groupImagePath:'http://185.82.23.22:3001/groupImages/'+file.filename,

            };
            res.jsonp(data);
        }
    else
        {
            console.log('File saved successfully.');
            var data = {
               message: 'Problem saving the file. Please try again.',
                errorCode : 400
            };
            res.jsonp(data);
        }

});

// Input data expected is {"deviceToken":100,"sendMessage":0} and then encode in url.
app.get('/setUserStatus', function (req, res) {
    var jsonData =  url.parse(req.url).query;
    var jsonObject = JSON.parse(decodeURIComponent(jsonData));
    if (jsonObject.deviceToken != "" || jsonObject.deviceToken != null )
    {
        userPushMessage.findOne({ deviceToken: jsonObject.deviceToken }, function(err, _deviceData) {
            if (err){
                            return res.json({
                                errDetails: err,
                                errorcode: 505,
                                success: false
                            });
                        }
            else{

                if (_deviceData != null )
                {
                    // token id exist in this case update the flag
                     var conditions = {deviceToken: jsonObject.deviceToken}, update = {sendMessage:jsonObject.sendMessage}, options = { multi: true };
                     userPushMessage.update(conditions, update, options, callback);

                     function callback (err, _numAffected) {
                          return res.json({msg: "Status of devices updated successfully.",
                                  errorcode: 200,
                                  numAffected:_numAffected,
                                  success: true});
                            }
                }
                else{
                     var _pushMsg = new userPushMessage({deviceToken:jsonObject.deviceToken ,sendMessage:jsonObject.sendMessage});
                      _pushMsg.save(function(_errorPushMsg){
                        if(_errorPushMsg)
                        {
                            return res.json({
                                errDetails: _errorPushMsg,
                                errorcode: 505,
                                success: false
                            });
                        }
                        else{
                            return res.json({msg: "Status of devices Inserted successfully.",
                                  errorcode: 200,
                                  success: true});

                        }
                            });
                }

            }

            });
    }
});

app.post('/createGroup', cors(),function (req, res) {
console.log("Welcome");
var jsonData =  url.parse(req.url).query;
 var jsonObject = JSON.parse(decodeURIComponent(jsonData));
 var newGroup = new Groups({name: jsonObject.groupName,  avtarImage:jsonObject.groupImage,createdByName: jsonObject.createdByName, createrId: jsonObject.createrId, userId:jsonObject.userId});
 newGroup.save(function(err){
						if(err)
                        {
                            return res.status(500).json({
                                errDetails: err,
                                errorcode: 505,
                                success: false
                            });
                        }
                        else{
                            // If there is no error.

                                for ( var i=0; i<  jsonObject.groupMember.length; i++ )
                                {
                                    var eachGroupMember = jsonObject.groupMember[i];
                                    var userId = eachGroupMember.userID;
                                    var phoneNumber = eachGroupMember.phoneNumber;
                                    var newGroupMember = new GroupsMembers({ groupId:newGroup.id, userNumber:phoneNumber,userId:userId } );
                                    newGroupMember.save(function(errSave){

                                    });
                                }
                            console.log("Done");
                            return res.status(200).json({
                                  msg: "Group created successfully.",
                                  errorcode: 200,
                                  groupImagePath:'http://162.243.225.225:3001/groupImages/',
                                  data : newGroup,
                                  groupId: newGroup.id,
                                  success: true });

                        }
					});


});


app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});
app.get('/indexGroup', function(req, res){
	res.sendFile(__dirname + '/indexGroup.html');
});

app.get('/demo', function(req, res){
	res.sendFile(__dirname + '/demo.html');
});
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', "http://"+req.headers.host+':3001');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  next();
  }
);

io.sockets.on('connection', function(socket){
    
	socket.on('new user', function(data, callback){
       registeruser.find().where({userId:data}).exec(function (err, docs)
        {
         console.log("DOCS == > "+ docs);
         if(docs=='')
         { 
            var newUser = new registeruser({userId:data,status:'Online'});
               console.log("New User ==>"+ newUser);
               newUser.save(function(err){
                      if(!err)
                      {
                           console.log('New User Save to Database ');
                      }
                      else
                      {
                          console.log("Error");
                          console.log(err);
                      }

                    });
         }
       });
        console.log("this is my new user"+data)
		if (data in users){
            console.log("data exist in user");
			callback(true);
		} else{
		console.log("data doesnot exist in user");
			socket.nickname = data;
			users[socket.nickname] = socket;
            console.log("receiver we got is "+users[socket.nickname])
          	Chat.find().where({receiver:data}).exec(function abc(err, docs){
					if(err) throw err;
					console.log('emit here========');
					socket.emit('img received', docs);
					//socket.emit('load old msgs', docs);
                callback(true);
                 console.log("emitted  data is "+docs)
					updateNicknames();
				});
		}
	});

    socket.on('request load old msgs', function(data, callback){

		socket.nickname = data;
        console.log("ios requested for offline message  for "+data);
        Chat.find().where({receiver:data}).exec(function abc(err, docs){
        if(err) callback(false) ;
           // socket.emit('load old msgs', docs);
             console.log("send data to iOS "+docs);
            callback(docs);
        });

	});

    socket.on('get oldmsg done', function(data){

		Chat.remove({receiver:data.receiver}).where({imgFlag:0}).exec();
	});

    socket.on('get img done', function(data){
        //delete from tbl...where({imgflg==0})0 for msgs
        //remove chat only dont remove imgs.
        for (var i = 0; i < data.length; i++) 
        {
            //data[i]
            console.log('its i>>',data[i]._id);
            console.log('its img>>',data[i].img);
            //Chat.update({id:data[i].id},{isImgDownloaded:1}).exec();
            var conditions = {id:data._id}, update = {isImgDownloaded:1}, options = { multi: true };
            Chat.update(conditions, update, options, callback);
            function callback (err, numAffected) 
            {       }
		}
	});
	
    socket.on('typing', function(data, callback){
        if(data.oppUser in users)
        {
            console.log("userlist &&&& ==== "+ users);
            users[data.oppUser].emit('typing',{oppUser:data.oppUser,currentUser:data.currentUser});
            console.log('typing.....');
        }
    });

    socket.on('stop typing', function(data, callback){
        if(data.oppUser in users)
        {
            users[data.oppUser].emit('stop typing');
            console.log('Stop typing ..... ');
        }
    });



// *************** Group Messages Start ***********************  //


    socket.on('send group Messages', function(_data, callback){
        var data = _data;
        var msg = data.message;
		var name= data.name;
        var senderName =  data.senderName;
		var sendImage = data.senderImage;
        var groupId = data.groupId;
        var gName = data.name;
        var gImage = data.groupImage;
		console.log("EMITEED DATA FROM CLIENT :-- "+ JSON.stringify(data));
        console.log('called send Group Message and trimming message is: ' + msg);
        GroupsMembers.find().where({groupId:groupId}).exec(function getAllusers(err, docs){
        if (!err )
        {

     		console.log("docs.length == "+docs.length);
            for(var i=0;i < docs.length; i++)
            {
                user2BroadCast = docs[i];
                if(user2BroadCast != null)
                    {
                        console.log("not null");
                        console.log(user2BroadCast.userNumber+"Dfgdfgdg");
                        if(user2BroadCast.userNumber in users)
                        {
                             console.log('sending message to user!'+JSON.stringify({message: msg, senderId: socket.nickname,senderName:senderName ,groupId:groupId ,groupName:gName,groupImage:gImage, isType:data.isType }));
                            // users[user2BroadCast.userNumber].emit('whisperGroup', {msg: msg, nick: socket.nickname, groupId:groupId , isType:data.isType });
                            users[user2BroadCast.userNumber].emit('whisperGroup', {message: msg, senderId: socket.nickname,senderName:senderName , senderImage:sendImage ,groupId:groupId ,groupName:gName,groupImage:gImage, isType:data.isType });

				       }
                        else
                        {
                        console.log('Opposite user is offline hence storing in db');
                        //send push message
                        console.log("msg === ***** "+msg);
                        console.log("user2BroadCast.userNumber == "+user2BroadCast.userNumber);
                        var unirest = require('unirest');
                        console.log("Reached === unirest" );
                        unirest.post('http://162.243.225.225:8000/send')
                        .headers({'Accept': 'application/json', 'Content-Type': 'application/json'})
                        .send({"users":[user2BroadCast.userNumber],"android": { "collapseKey": "optional", "data": {  "message": msg   } }})
                        .end(function (response)
                        {
                            console.log("response getiing !!!!"+response);
                        });
                        console.log("Notification Send >>> !!!");
                            
                        console.log("sender name:"+senderName);
                        console.log("sender image:"+sendImage);    
                   var newMsg = new Chat({msg:msg,groupId:groupId,groupName:gName,img:gImage, nick: socket.nickname, receiver:user2BroadCast.userNumber,imgFlag:0,senderName:senderName , senderImage:sendImage});
                   console.log("New Msg ==>"+ newMsg);
                   newMsg.save(function(err){
                          if(!err)
                          {
                               console.log('Chat Saved to Database ');
                          }
                          else
                          {
                              console.log("Error");
                              console.log(err);
                          }

                        });
                        }
                    }
            }
        }
    });
});

    socket.on('Get Group Members',function(res_data,callback){
		
         var data = res_data,
         groupId = data.groupId,
         userId =data.userId;
         var groupAdmin = '';
        console.log("Res Data == "+JSON.stringify(data));
		GroupsMembers.find().where({groupId:groupId}).exec(function getAllusers(err, docs){
	    if (!err )
	    {
//            Groups.find().where({"_id":groupId}).exec(function(err1,docs1)
//            {
//                if(!err1)
//                    {
//                        console.log("sfsfsfsfsdfs  " + JSON.stringify(docs1));
//                       groupAdmin = docs1.userId; 
//                    }
//            });
		    console.log("UserId of Res.. === >> "+data.userId);
            console.log("Group Data ==>  " + docs);  
            console.log("Users == > "+users);   
            if(userId in users)
            {
                console.log("User Exits in Users ..!!!");
                var memberData = {
                    "memberList":docs,
                    "groupAdmin":groupAdmin
                };
                console.log("Data Emited to Receiver &&&& ___ "+JSON.stringify(memberData));
                users[userId].emit('Get Group Members',memberData); 
                console.log("Data Emited to Receiver &&&& ___ ");
            }
            else
            {
                console.log("User Not Exits in Users ..!!!");
//                socket.nickname = userId;
//                users[socket.nickname] = socket;
//                users[socket.nickname].emit('Get Group Members',docs); 
                
            }
    	}
	   });
    });

    socket.on('Remove Member From Group',function(res_data,callback){
        	//{'groupId':"132465897",'userId':'123456789'}

         var data = res_data,
         groupId = data.groupId,
         userId = data.userId;
        console.log("Res Data == "+JSON.stringify(data));
        GroupsMembers.remove().where({$and:[{"groupId":groupId},{"userId":userId}]}).exec(function(err,rep_data){
            console.log("Delete Responce Data ==>"+rep_data + " n == "+ JSON.parse(rep_data).n);
            var data = JSON.parse(rep_data);

        if(!err)
            {
                if(data.n)
                {
                    var resData = {
                    "message":"Member Removed ...",
                    "errorCode":200
                };
                console.log("resData == > "+JSON.stringify(resData));
                callback(resData);
                }
                else
                {
                     var resData = {
                    "message":"Something Wrong.. Nothing to Delete",
                    "errorCode":400
                };
                console.log("resData == > "+JSON.stringify(resData));
                callback(resData);
                }
            }
            else
            {
                var resData = {
                    "errDetail":err,
                    "errorCode":505
                };
                console.log("resData == > "+JSON.stringify(resData));
                callback(resData);
            }
        });

    });

    socket.emit('Add Group Member',function(res_data,callback){

        var data = res_data,
        groupId= data.groupId,
        userId = data.userId;

        var newGroupMember = new GroupsMembers({ groupId:groupId, userNumber:userId,userId:userId } );
        newGroupMember.save(function(errSave,dataSave){
            if(errSave)
                {
                    console.log("Error == "+errSave);
                    callback(errSave);
                }
                else
                {
                    console.log("Data == "+dataSave);
                    callback(dataSave);
                }
         });


    });



// *************** Group Messages End ***********************  //


	socket.on('send message', function(_data, callback){
        var data = _data,
            receiverId = data.receiverId;

        console.log("full data == "+data);
        console.log("receiverId  === "+receiverId );
		console.log('after trimming message is: ' + data.message);
            var msg = data.message.trim();
		if(msg.substr(0,3) === '/w '){
			msg = msg.substr(3);
			var ind = msg.indexOf(' ');
			if(ind !== -1){
				var name = msg.substring(0, ind);
				var msg = msg.substring(ind + 1);
				if(name in users){
                    console.log('sending message to user!' );
          users[name].emit('whisper', {msg: msg, nick: socket.nickname, groupId:0 , isType:data.isType });
					console.log('message sent is: ' + msg);
					console.log('Whisper!');
				} else{
					//insert data here
                    console.log('Opposite user is offline hence storing in db');
                     var unirest = require('unirest')
                        // POST a form with an attached file
                        console.log("Reached === unirest" +  socket.nickname );
                        unirest.post('http://162.243.225.225:8000/send')
                        .headers({'Accept': 'application/json', 'Content-Type': 'application/json'})
                        .send({"users":[receiverId],"android": { "collapseKey": "optional", "data": {  "message": msg   } }})
                        .end(function (response)
                        {
                            console.log("response getiing !!!!"+response);
                        });
					var newMsg = new Chat({msg: msg, img:null, nick: socket.nickname, receiver:name,imgFlag:0});
					newMsg.save(function(err){
						if(err) throw err;
						//io.sockets.emit('new message', {msg: msg, nick: socket.nickname});
                        console.log('Going out 1');
					});
				}
			} else{
                  console.log('Going out 2');
			}
		} else{
              console.log('Going out 3');
		}
          console.log('Going out 4');
	});

    socket.on('user image', function(msg,callback){
        console.log('on user img.....');
        fs.exists(__dirname + "/" + msg.imageMetaData, function (exists) {
                  if (!exists) {
                          fs.mkdir(__dirname + "/" + msg.imageMetaData, function (e) {
                           if (!e) {
                           console.log("Created new directory without errors." + client.id);

                               fs.writeFile(__dirname + "/" + msg.imageMetaData + "/" + msg.imageMetaData + ".jpg",           msg.imageData, function (err) {
                     if (err) {
                        console.log('Error writing file to when directory doesnt exist '+ __dirname + err);
                     //throw err;
                     }
                     });

                           } else {
                           console.log("Exception while creating new directory....");
                           }
                           });
                  } else {

                      fs.writeFile(__dirname + "/" + msg.imageMetaData + "/" + msg.imageMetaData + ".jpg",           msg.imageData, function (err) {
                     if (err) {
                        console.log('Error writing file when directory exist '+ __dirname + err);
                     //throw err;
                     }
                     });
                  }
                  }
                 );
          if(msg.toUserId in users){
            console.log(msg.toUserId);
            users[msg.toUserId].emit('user image',{image:msg.imageData, nick: socket.nickname});
            console.log('Whisper!');
        } else{
            console.log('***error--');
        }
    });

    socket.on('share contact',function(data,callback){
        var dtc=JSON.stringify(data.contactdetails);
        console.log("contactdetails>>>>>>"+dtc);
        if(data.toUserId in users)
        {
           users[data.toUserId].emit('share contact',{contact:data.contactdetails, nick: socket.nickname});
        }
        else
        {
            console.log('***error--');
        }
    });

    socket.on('User Last Seen Status',function(data,callback){        
        console.log("User ID for check status ==> " + data);
        if(data in users)
        {
            socket.emit('User Last Seen Status',{"status":"Online","value":"Online","tousrid":data});
        }
        else
        {            
            registeruser.find().where({userId:data}).exec(function (err, docs)
            {
               console.log("DOCS For Last seen status"+ docs);               
               if(!err)
               {
                   socket.emit('User Last Seen Status',{"status":"LastSeen","value":docs[0].updatedAt,"tousrid":docs[0].userId});
               }
            });
       }
    });  
    
	socket.on('disconnect', function(data){
        console.log("Disconnect Data "+JSON.stringify(data));        
        console.log("@@@@@@@@@@@@@@@@##################@@@@@@@@@@@@###########");
        console.log(" DIS Socket Nick ==> "+socket.nickname);
        var conditions = {userId:socket.nickname }, update = {updatedAt:Date.now,status:"Offline"}, options = { multi: true };
            registeruser.update(conditions, update, options, callback);
            function callback (err, _numAffected) 
            {
                console.log("Status of User updated successfully.");
            }
    
		if(!socket.nickname) return;
		delete users[socket.nickname];
		updateNicknames();
	});
    
    function updateNicknames()
    {
		io.sockets.emit('usernames', Object.keys(users));
	}
});

